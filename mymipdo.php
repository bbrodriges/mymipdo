<?php

class MyMiPDO {

	protected $DBCONNECT; // connection to the database

    public function __construct() {
        $db_host = 'localhost';
        $db_database = 'my_db';
        $db_login = 'login';
        $db_password = 'password'
        $this->DBCONNECT = new PDO('mysql:host='.$db_host.';dbname='.$db_database.';charset=UTF8', $db_login, $db_password
                                    /*, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")*/
                                    /*, array(PDO::ATTR_PERSISTENT => true)*/
                                  );
        // uncomment MYSQL_ATTR_INIT_COMMAND to set defalt names in utf8
        // uncomment ATTR_PERSISTENT to lower number of connections);
    }

	// execute query and returns result
	// param $query must be string
	public function dbQuery($query) {
		$preparation = $this->DBCONNECT->prepare($query);
		$preparation->execute();
		return $preparation;
	}

	// execute query and returns one column result
	// param $query must be string
	public function dbFetchColumn($query, $column = 0) {
		return $this->dbQuery($query)->fetchColumn($column);
	}

	// execute query and returns row as object
	// param $query must be string
	public function dbFetchObject($query) {
		$result = $this->dbQuery($query);
		if($result){
			return $result->fetchObject();
		}
		return false;
	}

	// execute query and returns an array containing all of the result set rows
	// param $query must be string
	public function dbFetchAll($query) {
		$result = $this->dbQuery($query);
		if($result){
			return $result->fetchAll();
		}
		return false;
	}

	// execute query and returns number of rows in table with optional condition
	// param $table must be string
	// param $condition must be string
	public function dbRowCount($table, $condition='') {
		if($condition)
			$count = $this->dbQuery('SELECT COUNT(id) FROM '.$table.' WHERE '.$condition);
		else
			$count = $this->dbQuery('SELECT COUNT(id) FROM '.$table);
		if($count){
			$count = $count->fetchAll();
			return $count[0][0]; // first key of first array has our count result
		}
		return false; 
	}

	// execute query and returns id of fist or last row in table
	// param $table must be string
    // param $condition must be "first" or "last"
	public function dbTableID($table, $condition='last') {
        $order = 'DESC';
        if($condition!='last')
            $order = 'ASC';
		return $this->dbFetchObject('SELECT id FROM '.$table.' ORDER BY id '.$order.' LIMIT 1')->id;
	}

    // execute query and returns id of last element in table
    // param $table must be string
    public function dbLastInsert($table) {
        return $this->dbFetchObject('SELECT * FROM '.$table.' ORDER BY id DESC LIMIT 1');
    }


}
?>