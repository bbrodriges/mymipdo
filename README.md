mymipdo
=======

MyMiPDO (My Micro PDO) is a simple class with basic usefull methods to operate with databases using PHP PDO.

How to use it
-------

Change default database info and user credentials in mymipdo.php file.

``` php
<?php
include('mymipdo.php')

$db = new MyMiPDO();

//all records from movies table
$all_my_movies = $db->dbFetchAll('SELECT * FROM movies');
//latest inserted record into movies table
$last_inserted_movie = $db->dbLastInsert('movies');
//first and last ids in movies table
$first_movie_id = $db->dbTableID('movies', 'first');
$last_movie_id = $db->dbTableID('movies', 'last');
//one exact movie from movies table
$the_one_and_only_movie = $db->dbFetchObject('SELECT * FROM movies WHERE title="The Shawshank Redemption"');
?>
```

License
-------

Do with this files and code whatever you want :)
